from django.conf.urls import patterns, url

from googleds import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^corpus/', views.corpus, name='corpus'),
	url(r'^download/', views.download, name='download'),
	url(r'^getlist/', views.getlist, name='getlist'),
	url(r'^author/(.*)$', views.author, name="author"),
)