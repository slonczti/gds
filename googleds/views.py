from django.http import HttpResponse
from django.core.servers.basehttp import FileWrapper
from django.core.files import File
from django.shortcuts import render
from django.template import RequestContext, loader, Library
import json
import os
import random
import pairtree
import requests
import mimetypes
from zipfile import ZipFile
from StringIO import StringIO

main_dir = os.path.dirname(__file__)+"/static/"
bib_level_dict = {"a":"Monographic Component Part", "b":"Serial Component Part", "c":"Collection", "d":"Subunit", "i":"Integrating Resource", "m":"Monograph/Item", "s":"Serial"}
literary_form_dict = {"0":"Not Fiction", "1":"Fiction","d":"Dramas","e":"Essays","f":"Novels","h":"Humor, satires, etc.","i":"Letters","j":"Short Stories","m":"Mixed forms","p":"Poetry","s":"Speeches","u":"Unknown","|":"Undefined"}
type_of_record_dict = {"a":"Language Material","c":"Notated music","d":"Manuscript notated music","e":"Cartographic Material","f":"Manuscript cartographic material","g":"Projected medium","i":"Nonmusical sound recording","j":"Musical sound recording","k":"Two-dimensional nonprojectable graphic","m":"Computer file","o":"Kit","p":"Mixed materials","r":"Three-dimensional artifact or naturally occurring object","t":"Manuscript language material"}
nature_of_contents_dict = {"a":"Abstracts/summaries","b":"Bibliographies","c":"Catalogs","d":"Dictionaries","e":"Encyclopedias","f":"Handbooks","g":"Legal articles","i":"Indexes","j":"Patent document","k":"Discographies","l":"Legislation","m":"Theses","n":"Surveys of literature in a subject area","o":"Reviews","q":"Programmed texts","r":"Directories","s":"Statistics"}

def index(request):
		
	corpus_options = {}
	corpus_options["nature_of_contents"] = Get_Values("nature_of_contents_keys.json", nature_of_contents_dict)
	corpus_options["literary_forms"] = Get_Values("literary_forms_keys.json", literary_form_dict)
	corpus_options["bib_level"] = Get_Values("bib_level_keys.json", bib_level_dict)
	corpus_options["type_of_record"] = Get_Values("type_of_record_keys.json", type_of_record_dict)
	corpus_options["language_dict"] = languages_loader()
	
	return render(request, 'googleds/index.html', corpus_options)
	
def languages_loader():
	languages = []
	top_languages = ["eng","ita","dut","ger","jpn","chi","fre"]
	with open(main_dir+"languages_keys.json") as f:
		language_set = json.load(f)
	with open(main_dir+"loc_codes.json") as g:
		language_codes = json.load(g)
	language_set.remove("   ")
	for language in language_set:
		if language in top_languages: 
			language_set.remove(language)
		elif language in language_codes:
			languages.append((language,language_codes[language]))

	return sorted(languages)

def Get_Values(filename, dictionary):

	with open(main_dir+filename) as f:
		values_set = json.load(f)
	bib_levels = []
	for value in values_set:
		if value in dictionary:
			bib_levels.append((value,dictionary[value]))		

	return bib_levels
	
def author(request, author):
	corpus = {}
	with open(main_dir+"authors.json") as f:
		author_data = json.load(f)
	results = author_data[author]
	
	request.session["id"] = os.urandom(8).encode('hex')
	with open(main_dir+"tmp_results"+request.session["id"]+".json", "w") as f:
		output_data = json.dumps(results)
		f.write(output_data)

	#corpus["results"] = results
	count = len(results)
	corpus["length"] =  count
	
	if count > 50:
		corpus["sample_results"] = 50
	else:
		corpus["sample_results"] = count
		
	if count <> 0:
		details = []
		with open(main_dir+"basic_info.json") as f:
			basic_data = json.load(f)
		randoms = random.sample(xrange(0,count),corpus["sample_results"])
		for i in randoms:
			item = results[i]
			details.append((item, basic_data[item][0], basic_data[item][1], basic_data[item][2]))
		corpus["details"] = details
	return render(request, "googleds/corpus.html", corpus)
	
	
def corpus(request):
	data_types = ["dates","language","nature_of_contents","type_of_record","bib_level","literary_form","author"]
	sets = ["record_set","date_set","bib_set", "author_set", "literary_set", "contents_set","language_set"]
	corpus = {}
	corpus_builder = {}
	
	a = request.GET
	
	corpus["language"] = a.getlist("language")
	corpus["literary_form"] = a.getlist("form")
	corpus["type_of_record"] = a.getlist("type")
	corpus["year1"] = a.get("year1")
	corpus["year2"] = a.get("year2")
	corpus["nature_of_contents"] = a.getlist("contents")
	corpus["bib_level"] = a.getlist("level")
	corpus["author"] = a.get("author")
	"""
	for type in data_types:
		
		if corpus[type] == "None":
			pass
		else:	
			data_list = []
			with open(main_dir+type+".json") as f:
				data = json.load(f)
			for item in corpus[type]:
				data_list += data[item]
	"""	
	#grabbing record type data
	if corpus["type_of_record"] == ["all"]:
		record_set = None
	else:
		with open(main_dir+"type_of_record.json") as f:
			record_data = json.load(f)
	
		record_list = []
		for type in corpus["type_of_record"]:
			record_list += record_data[type] 
		record_set = set(record_list)
	
	#grabbing year data
	if corpus["year1"] == "" and corpus["year2"] == "":
		date_set = None
	else:
		with open(main_dir+"dates.json") as f:
			date_data = json.load(f)
	
		date_list = []
		current_year = eval(corpus["year1"])
		while current_year <= eval(corpus["year2"]):
			if str(current_year) in date_data:
				date_list += date_data[str(current_year)]
			current_year += 1 
		date_set = set(date_list)
		
	#grabbing bib_level data
	if corpus["bib_level"] == ["all"]:
		bib_set = None
	else:	
		with open(main_dir+"bib_level.json") as f:
			bib_data = json.load(f)
	
		bib_level_list = []
		for level in corpus["bib_level"]:
			bib_level_list += bib_data[level] 
		bib_set = set(bib_level_list)
		
	#grabbing author information
	author_set = None
	if corpus["author"] <> "":
		
		with open(main_dir+"authors.json") as f:
			author_data = json.load(f)
		if corpus["author"] in author_data:
			author_set = set(author_data[corpus["author"]])
		
	#grabbing literary form data
	literary_set = None	
	if corpus["literary_form"] <> ["None"]:
		with open(main_dir+"literary_forms.json") as f:
			literary_data = json.load(f)
	
		literary_list = []
		for form in corpus["literary_form"]:
			literary_list += literary_data[form] 

		literary_set = set(literary_list)
	
	#grabbing literary form data	
	contents_set = None
	if corpus["nature_of_contents"] <> ["None"]:
		with open(main_dir+"nature_of_contents.json") as f:
			contents_data = json.load(f)
	
		contents_list = []
		for form in corpus["nature_of_contents"]:
			contents_list += contents_data[form] 

		contents_set = set(contents_list)
	
	#grabbing language data
	with open(main_dir+"languages.json") as f:
		language_data = json.load(f)
	
	language_list = []
	for language in corpus["language"]:
		language_list = language_list + language_data[language] 
	language_set = set(language_list)

	results_set = [x for x in sets if eval(x) is not None]
	results = list(set.intersection(*[eval(x) for x in results_set]))
	
	request.session["id"] = os.urandom(8).encode('hex')
	with open(main_dir+"tmp_results"+request.session["id"]+".json", "w") as f:
		output_data = json.dumps(results)
		f.write(output_data)

	
	length = len(results)
	corpus["length"] =  length
	
	count = eval(a.get("count"))
	corpus["sample_results"] = count
	
	if a.get("show_results") == "show" and length > 0:
		corpus["details"] = Get_Details(length,count, results)
	else:
		corpus["details"] = None
	return render(request, "googleds/corpus.html", corpus)
	
def Get_Details(length, count, results):
     
	details = []
	with open(main_dir+"basic_info.json") as f:
		basic_data = json.load(f)
		if length > count:
			randoms = random.sample(xrange(0,length),count)
			for x in randoms:
				item = results[x]
				details.append((item, basic_data[item][0], basic_data[item][1], basic_data[item][2]))
			
		else:
			for item in results:
				details.append((item, basic_data[item][0], basic_data[item][1], basic_data[item][2]))
			
	return details
	
	

def delete_file(name):
	if os.path.isfile(main_dir+name):
		os.remove(main_dir+name)
	
def download(request):

	delete_file("gds.zip")
	with open(main_dir+"tmp_results"+request.session["id"]+".json", "r") as f:
		download_data = json.load(f)
	
	downloads = {}

	a = request.GET
	if "count" in a:
		randoms = random.sample(xrange(0,len(download_data)),int(a["count"]))
		download_random = [download_data[x] for x in randoms]
		download_data = download_random
	
	for id in download_data:
		
		id_parts = id.split(".")
		id_pairtree = pairtree.id2path(id_parts[1])
		pairtree_string = id_pairtree.replace("/","")[:-3]
		path = id_parts[0]+"/pairtree_root/"+id_pairtree[:-4]+2*("/"+pairtree_string)+".zip"
		
		r=requests.get("http://archive.lib.msu.edu/GDS/"+path)
		
		if r.ok:
			with ZipFile(main_dir+"gds.zip","a") as dlzip:
				dlzip.writestr(id.replace("/","")+".zip",r.content)
	
	with open(main_dir+"gds.zip") as f:
		myfile = File(f)
	
	dl_file = main_dir+"gds.zip"
	filename = os.path.basename(dl_file)
	response = HttpResponse(FileWrapper(open(dl_file)), 
							content_type=mimetypes.guess_type(dl_file)[0])
	response["Content-Length"] = os.path.getsize(dl_file)
	response["Content-Disposition"] = "attachment; filename=%s" % filename
	return response
	
	#downloads["paths"] = paths
	#downloads["rs"] = rs
	#downloads["size"] = myfile.size
	#return render(request, "googleds/download.html", downloads)

def getlist(request):

	delete_file("list.txt")
	with open(main_dir+"tmp_results"+request.session["id"]+".json", "r") as f:
		id_list = json.load(f)
		
	with open(main_dir+"list.txt", "w") as f:
		for x in id_list:
			f.write(x+"\n")
			
	dl_file = main_dir+"list.txt"
	
	filename = os.path.basename(dl_file)
	response = HttpResponse(FileWrapper(open(dl_file)), 
							content_type=mimetypes.guess_type(dl_file)[0])
	response["Content-Length"] = os.path.getsize(dl_file)
	response["Content-Disposition"] = "attachment; filename=%s" % filename
	
	return response		
	
def download1(request):
	a = request.GET
	test = {}
	if "count" in a:
		test["test"] = a["count"]
	else:
		test["test"] = "success"
	return render(request, "googleds/download.html", test)